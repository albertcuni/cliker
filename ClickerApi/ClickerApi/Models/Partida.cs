﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClickerApi.Models
{
    public class Partida
    {
        public string Usuario { get; set; }
        public int punts { get; set; }
        public int wats { get; set; }
        public int millora1 { get; set; }
        public int millora2 { get; set; }
        public int millora3 { get; set; }
        public int millora4 { get; set; }
        public int millora5 { get; set; }
        public int millora6 { get; set; }
    }
}