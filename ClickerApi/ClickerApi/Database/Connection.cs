﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ClickerApi.Database
{
    public class Connection
    {
        private string StringConnection = "Data Source=oracle.ilerna.com;Initial Catalog=DAM2_CUNILLERABENETALBERT_Clicker; User ID=DAM2_39528338D;Password=Albertcuni11*";
        private SqlConnection connection;
        private SqlCommand cmd;

        public void Open()
        {
            try
            {
                connection = new SqlConnection(StringConnection);
                connection.Open();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void Close()
        {
            connection.Close();
        }
        public void ExecuteNonQuery(string sql)
        {
            if (cmd == null)
            {
                cmd = new SqlCommand();
            }

            cmd.Connection = connection;
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
        }
        public SqlDataReader Select(string sql)
        {
            if (cmd == null)
            {
                cmd = new SqlCommand();
            }
            cmd.Connection = connection;
            cmd.CommandText = sql;
            //SqlDataReader devuelve los datos
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

    }
}