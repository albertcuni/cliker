﻿using Cliker.Services;
using Cliker.Model;
using Cliker.Service;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Cliker
{
    public class MainPageViewModel : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand CommandClick { get; set; }
        public ICommand ComandoMejora1 { get; set; }
        public ICommand ComandoReset { get; set; }
        public ICommand ComandoDatosPartida { get; set; }

        public ICommand ComandoMejora2 { get; set; }
        public ICommand Bonus2 { get; set; }
        public ICommand Bonus3 { get; set; }
        public ICommand BotonLogin { get; set; }
        public ICommand BotonRegistre { get; set; }

        public ICommand ComandoMejora3 { get; set; }
        public ICommand ComandoMejora4 { get; set; }
        public ICommand ComandoMejora5 { get; set; }
        public ICommand ComandoMejora6 { get; set; }



        public int numeroCliks;

        public int numeroMejoras1 = 1;
        public int preuMillora = 10;

        public int numeroMejoras2 = 1;
        public int preuMillora2 = 10;

        public int preuMillora3 = 30;
        public int numeroMejoras3 = 0;

        public int preuMillora4 = 40;
        public int numeroMejoras4 = 0;

        public int preuMillora5 = 50;
        public int numeroMejoras5 = 0;

        public int preuMillora6 = 60;
        public int numeroMejoras6 = 0;

       
        public int tapCount = 0;
        public String imatge = "plutoni1.png";
        public String colorMillora1 = "Green";
        public String colorMillora2 = "Green";
        public Boolean autocont = true;
        public int randomoculta = 0;

        public int randomHoritzontal = 0;
        public int randomVertical = 0;

        public String imatgeoculta = "False";

        public int randomoculta2 = 0;
        public Boolean boolbonus2 = false;

        public int randomHoritzontal2 = 0;
        public int randomVertical2 = 0;

        public String imatgeoculta2 = "False";
        
        public String imatgebonus = "joc.jpg";

        public int millon = 0;
        public int billons = 0;
        public int trillon = 0;
        public int cuadrillon = 0;
        public int quintillon = 0;
        public int sextillon = 0;
        public int septillon = 0;
        public int octillon = 0;
        public int nonillon = 0;
        public int decallon = 0;
        public String User
        {
            set
            {
                if (user != value)
                {
                    user = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("User"));
                    }
                }
            }
            get { return user; }
        }
        public String user = "";
        public String Pass
        {
            set
            {
                if (pass != value)
                {
                    pass = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Pass"));
                    }
                }
            }
            get { return pass; }
        }
        public String pass = "";

        public String Name
        {
            set
            {
                if (name != value)
                {
                    name = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                    }
                }
            }
            get { return name; }
        }
        public String name = "";
        public String Surname
        {
            set
            {
                if (surname != value)
                {
                    surname = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Surname"));
                    }
                }
            }
            get { return surname; }
        }
        public String surname = "";
        public String Email
        {
            set
            {
                if (email != value)
                {
                    email = value; if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Email"));
                    }
                }
            }
            get { return email; }
        }
        public String email = "";

        ContentPage nav;

        public int NumeroCliks
        {      set
            {
                if (numeroCliks != value) { numeroCliks = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("NumeroCliks")); } }
            }
            get { return numeroCliks; }
        }
        public int NumeroMejoras1
        {
            set
            {
                if (numeroMejoras1 != value) { numeroMejoras1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("NumeroMejoras1")); } }
            }
            get { return numeroMejoras1; }
        }
        public int PreuMillora
        {
            set
            {
                if (preuMillora != value) { preuMillora = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora")); } }
            }
            get { return preuMillora; }
        }
        public String ColorMillora1
        {
            set
            {
                if (colorMillora1 != value) { colorMillora1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("ColorMillora1")); } }
            }
            get { return colorMillora1; }
        }
        public String Imatge
        {
            set
            {
                if (imatge != value) { imatge = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Imatge")); } }
            }
            get { return imatge; }
        }
        public int NumeroMejoras2
        {
            set
            {
                if (numeroMejoras2 != value) { numeroMejoras2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("NumeroMejoras2")); } }
            }
            get { return numeroMejoras2; }
        }
        public int PreuMillora2
        {
            set
            {
                if (preuMillora2 != value) { preuMillora2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora2")); } }
            }
            get { return preuMillora2; }
        }
        public String ColorMillora2
        {
            set
            {
                if (colorMillora2 != value) { colorMillora2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("ColorMillora2")); } }
            }
            get { return colorMillora2; }
        }
        public int RandomHoritzontal
        {
            set
            {
                if (randomHoritzontal != value) { randomHoritzontal = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("RandomHoritzontal")); } }
            }
            get { return randomHoritzontal; }
        }
        public int RandomVertical
        {
            set
            {
                if (randomVertical != value) { randomVertical = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("RandomVertical")); } }
            }
            get { return randomVertical; }
        }
        public String Imatgeoculta
        {
            set
            {
                if (imatgeoculta != value) { imatgeoculta = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Imatgeoculta")); } }
            }
            get { return imatgeoculta; }
        }

        public int RandomHoritzontal2
        {
            set
            {
                if (randomHoritzontal2 != value) { randomHoritzontal2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("RandomHoritzontal2")); } }
            }
            get { return randomHoritzontal2; }
        }
        public int RandomVertical2
        {
            set
            {
                if (randomVertical2 != value) { randomVertical2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("RandomVertical2")); } }
            }
            get { return randomVertical2; }
        }
        public String Imatgeoculta2
        {
            set
            {
                if (imatgeoculta2 != value) { imatgeoculta2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Imatgeoculta2")); } }
            }
            get { return imatgeoculta2; }
        }
        public int NumeroMejoras3
        {
            set
            {
                if (numeroMejoras3 != value) { numeroMejoras3 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("NumeroMejoras3")); } }
            }
            get { return numeroMejoras3; }
        }
        public int PreuMillora3
        {
            set
            {
                if (preuMillora3 != value) { preuMillora3 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora3")); } }
            }
            get { return preuMillora3; }
        }

        public int NumeroMejoras4
        {
            set
            {
                if (numeroMejoras4 != value) { numeroMejoras4 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("NumeroMejoras4")); } }
            }
            get { return numeroMejoras4; }
        }
        public int PreuMillora4
        {
            set
            {
                if (preuMillora4 != value) { preuMillora4 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora4")); } }
            }
            get { return preuMillora4; }
        }
        public int NumeroMejoras5
        {
            set
            {
                if (numeroMejoras5 != value) { numeroMejoras5 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("NumeroMejoras5")); } }
            }
            get { return numeroMejoras5; }
        }
        public int PreuMillora5
        {
            set
            {
                if (preuMillora5 != value) { preuMillora5= value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora5")); } }
            }
            get { return preuMillora5; }
        }

        public int NumeroMejoras6
        {
            set
            {
                if (numeroMejoras6 != value) { numeroMejoras6 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("NumeroMejoras6")); } }
            }
            get { return numeroMejoras6; }
        }
        public int PreuMillora6
        {
            set
            {
                if (preuMillora6 != value) { preuMillora6 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PreuMillora6")); } }
            }
            get { return preuMillora6; }
        }

        public String Imatgebonus
        {
            set
            {
                if (imatgebonus != value) { imatgebonus = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Imatgebonus")); } }
            }
            get { return imatgebonus; }
        }

        public MainPageViewModel(ContentPage navi)
        {
            nav = navi;
            CommandClick = new Command(SumadorCliks);
            ComandoMejora1 = new Command(MilloraCliks);
            ComandoReset = new Command(Reset);
            ComandoMejora2 = new Command(MilloraCliks2);
            Bonus2 = new Command(Bonusfuncio2);
            Bonus3 = new Command(Bonusfuncio3);


            ComandoMejora3 = new Command(MilloraCliks3);
            ComandoMejora4 = new Command(MilloraCliks4);
            ComandoMejora5 = new Command(MilloraCliks5);
            ComandoMejora6 = new Command(MilloraCliks6);
            BotonLogin = new Command(IniciarSesion);
            BotonRegistre = new Command(Registre);
            ComandoDatosPartida = new Command(DatosPartida);


            Oculta();
            RandomImage();
            Oculta2();
            RandomImage2();
        }
        private async Task SumaautoAsync()
        {

            while (autocont == true)
            {
                NumeroCliks = NumeroCliks + NumeroMejoras2;
                await Task.Delay(1000);

            }
        }
        public void SumadorCliks()
        {

            NumeroCliks = NumeroCliks + NumeroMejoras1;
            NumeroCliks = NumeroCliks + NumeroMejoras3;
            NumeroCliks = NumeroCliks + NumeroMejoras4;
            NumeroCliks = NumeroCliks + NumeroMejoras5;
            NumeroCliks = NumeroCliks + NumeroMejoras6;
            if (tapCount == 0)
            {
                Imatge = "plutoni.png";
                tapCount = 1;

            }
            else
            {
                Imatge = "plutoni1.png";
                tapCount = 0;


            }
            if(boolbonus2 == true)
            {
                NumeroCliks = numeroCliks + 500;
                imatgebonus = "fondo_millora.png";
            }
        }
        public void MilloraCliks()
        {

            if (NumeroCliks >= PreuMillora)
            {
                NumeroCliks = NumeroCliks - PreuMillora;
                NumeroMejoras1++;

                PreuMillora = PreuMillora * 2;

            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Gamarús", "A per mes clicks!!!! ", "Continua Jugant ");
            }
        }
        public void MilloraCliks2()
        {

            if (NumeroCliks >= PreuMillora2)
            {
                autocont = true;

                if (PreuMillora2 == 10)
                {
                    SumaautoAsync();
                }

                NumeroCliks = NumeroCliks - PreuMillora2;
                NumeroMejoras2++;

                PreuMillora2 = PreuMillora2 * 2;

            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Gamarús", "A per mes clicks!!!! ", "Continua Jugant ");
            }
        }
        private async Task RandomImage()
        {
            while (true)
            {
                Random rand = new Random();
                RandomHoritzontal = rand.Next(-150, 150);
                RandomVertical = rand.Next(-620, 50);
                await Task.Delay(1000);


            }

        }
        private async Task Oculta()
        {
            Random rand = new Random();
            while (true)
            {
                await Task.Delay(5000);

                randomoculta = rand.Next(1, 10);

                if (randomoculta == 1)
                {

                    Imatgeoculta = "True";
                }
                else
                {
                    Imatgeoculta = "False";
                }
            }
        }


        public void Bonusfuncio2()
        {
            NumeroCliks = NumeroCliks + (NumeroMejoras1 * 5);

        }

        public void Bonusfuncio3()
        {
            boolbonus2 = true;
            imatgebonus = "fondo_millora.png";
        }

        private async Task RandomImage2()
        {
            while (true)
            {
                Random rand = new Random();
                RandomHoritzontal2 = rand.Next(-150, 150);
                RandomVertical2 = rand.Next(-620, 50);
                await Task.Delay(1000);


            }

        }
        private async Task Oculta2()
        {
            Random rand = new Random();
            while (true)
            {
                await Task.Delay(1000);

                randomoculta2 = rand.Next(1, 10);

                if (randomoculta2 == 1)
                {

                    Imatgeoculta2 = "True";
                    await Task.Delay(1000);
                    Imatgeoculta2 = "False";
                    await Task.Delay(29000);
                    imatgebonus = "joc.jpg"; 
                     boolbonus2 = false;


                }
                else
                {
                    Imatgeoculta2 = "False";

                }
            }
        }
        public void MilloraCliks3()
        {

            if (NumeroCliks >= PreuMillora3)
            {
                numeroMejoras3 = 3;
                NumeroCliks = NumeroCliks - PreuMillora3;
                NumeroMejoras3++;

                PreuMillora3 = PreuMillora3 * 3;

            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Gamarús", "A per mes clicks!!!! ", "Continua Jugant ");
            }
        }

        public void MilloraCliks4()
        {

            if (NumeroCliks >= PreuMillora4)
            {
                numeroMejoras4 = 4;
                NumeroCliks = NumeroCliks - PreuMillora4;
                NumeroMejoras4++;

                PreuMillora4 = PreuMillora4 * 4;

            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Gamarús", "A per mes clicks!!!! ", "Continua Jugant ");
            }
        }
        public void MilloraCliks5()
        {

            if (NumeroCliks >= PreuMillora5)
            {
                numeroMejoras5 = 5;
                NumeroCliks = NumeroCliks - PreuMillora5;
                NumeroMejoras5++;

                PreuMillora5 = PreuMillora5 * 5;

            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Gamarús", "A per mes clicks!!!! ", "Continua Jugant ");
            }
        }
        public void MilloraCliks6()
        {

            if (NumeroCliks >= PreuMillora6)
            {
                numeroMejoras6 = 6;
                NumeroCliks = NumeroCliks - PreuMillora6;
                NumeroMejoras6++;

                PreuMillora6 = PreuMillora6 * 6;

            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Gamarús", "A per mes clicks!!!! ", "Continua Jugant ");
            }
        }

        private async void IniciarSesion()
        {
            UserService service = new UserService();
            Boolean bol = true;
            if (String.IsNullOrWhiteSpace(User))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Pass))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (bol == true)
            {
                bool isLogued = await service.Login(User, Pass);

                if (isLogued)
                {
                    await nav.Navigation.PushModalAsync(new MainPage(this));
                }
            }
        }
        private async void Registre()
        {
            UserService service = new UserService();
            Boolean bol = true;
            if (String.IsNullOrWhiteSpace(Name))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Surname))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Email))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(User))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }
            if (String.IsNullOrWhiteSpace(Pass))
            {
                await this.DisplayAlert("Advertencia", "Este campo es obligatorio", "Cerrar");
                bol = false;
            }

            User usuario = new User();
            usuario.Nombre = Name;
            usuario.Apellido = Surname;
            usuario.Email = Email;
            usuario.Usuario = User;
            usuario.Password = Pass;


            if (bol == true)
            {
                bool isLogued = await service.Registre(usuario);

                if (isLogued)
                {
                    await nav.Navigation.PushModalAsync(new Login(this));
                }
            }
        }
     
        private async void DatosPartida()
        {
            PartidaService service = new PartidaService();

            Partida usuario = new Partida();
            usuario.Usuario = User;
            usuario.punts = NumeroCliks;
            usuario.wats= NumeroMejoras1;
            usuario.millora1 = NumeroMejoras2;
            usuario.millora2 = NumeroMejoras3;
            usuario.millora3 = NumeroMejoras4;
            usuario.millora4 = NumeroMejoras5;
            usuario.millora5 = NumeroMejoras6;
            service.Datos(usuario);
        }



        public void Reset()
        {
            
            NumeroCliks = 0;
            PreuMillora = 10;
            PreuMillora2 = 10;
            NumeroMejoras1 = 1;
            ColorMillora1 = "Green";
            ColorMillora2 = "Green";
            NumeroMejoras2 = 1;
            autocont = false;
            PreuMillora3 = 30;
            NumeroMejoras3 = 0;
            PreuMillora4 = 40;
            NumeroMejoras4 = 0;
            PreuMillora5 = 50;
            NumeroMejoras5 = 0;
            PreuMillora6 = 60;
            NumeroMejoras6 = 0;
        }

    }
}
