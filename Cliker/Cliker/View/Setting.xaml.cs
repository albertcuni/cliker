﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cliker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Setting : ContentPage
    {
        public Setting(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;

        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
        private void Registre(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Registre(BindingContext));
        }
        private void Login(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Login(BindingContext));
        }

    }
}