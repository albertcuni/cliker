﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Cliker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Millora : ContentPage
    {
        public Millora(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;

        }
        private void MainPage(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new MainPage(BindingContext));
        }
    }
}