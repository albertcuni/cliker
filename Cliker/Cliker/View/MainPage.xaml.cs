﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace Cliker
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public MainPage(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;
        }
        private void Millora(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Millora(BindingContext));
        }
        private void Setting(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Setting(BindingContext));
        }

    }
}

