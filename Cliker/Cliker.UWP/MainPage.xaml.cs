﻿namespace Cliker.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            LoadApplication(new Cliker.App());
        }
    }
}
